#!/usr/bin/python3
import sys
import re
import subprocess

if len(sys.argv) < 2:
    print("usage: python3 autopy3.py {interface} {IP adress}")
    print("example: python3 autopy3.py tap0 10.10.10.10")
    sys.exit()

else:
    print("[+] UNICORNSCAN running against target: %s" % (sys.argv[2]))
    print("[+] Logging the result to uscan_tcp.txt and uscan_udp.txt")

    subprocess.call(['unicornscan', '-r' ,'3000', '-l', 'uscan_tcp.txt',
                    '-i', sys.argv[1], '-msf', sys.argv[2]+":a"])

    subprocess.call(['unicornscan', '-r' ,'3000', '-mU', '-l', 'uscan_udp.txt',
                    '-i', sys.argv[1], sys.argv[2]+":a"])
    print("[+] UNICORNSCAN logs are ready to be fed into NMAP!")

    subprocess.getoutput(
        "cat uscan_tcp.txt | sed 's/\s\+//g' | cut -d '[' -f2 | cut -d ']' -f1 > t_ports.txt")

    subprocess.getoutput(
        "cat uscan_udp.txt | sed 's/\s\+//g' | cut -d '[' -f2 | cut -d ']' -f1 > u_ports.txt")

    print("[+] NMAP service scan started on open TCP and UDP ports")

    subprocess.getoutput(
        "nmap -sV -p $(tr '\n' , <t_ports.txt) -e %s %s -oN nmap_TCPscan.txt" % (sys.argv[1], sys.argv[2]))

    subprocess.getoutput(
        "nmap -sV -p $(tr '\n' , <u_ports.txt) -e %s %s -oN nmap_UDPscan.txt" % (sys.argv[1], sys.argv[2]))

    print("[+] NMAP scan finished. Log files are nmap_TCPscan.txt and nmap_UDPscan.txt")

print("[+] Do you want to activate service specific scans against the current target?")
print("[+] Enter: y or n ")
choice = input()

if choice == "n":
    print("Goodbye!")
    sys.exit()

elif choice == "y":
    with open("nmap_TCPscan.txt") as fnmap:

        for line in fnmap:

            try:
                check = re.search(
                    r'(^\d+)\/\w+\s+\w+\s+(https)|(^\d+)\/\w+\s+\w+\s+(http)', line)
                
                if check.group(4) == "http":
                    i = check.group(3)
                    print(
                        "[>] Activating Gobuster to run against HTTP port: %s" % str(i))
                    gob_http_log = open(
                        'gobust_http:%s_scan.txt' % i, 'w+b')
                    subprocess.call(['gobuster', '-w', '/usr/share/seclists/Discovery/Web-Content/common.txt',
                                    '-u', 'http://'+sys.argv[2]+':'+check.group(3)], stdout=gob_http_log)

                    print(
                        "[>] Activating Nikto to run against HTTP port: %s" % str(i))
                    nik_log = open('nikto_scan_%s.txt' % i, 'w+b')
                    subprocess.call(
                        ['nikto', '-h', 'http://'+sys.argv[2]+':'+check.group(3)], stdout=nik_log)

                elif check.group(4) == "https":
                    j = check.group(3)
                    print ("[>] Activating Gobuster to run against HTTPS port: %s" %str(j))
                    gob_https_log = open('gobust_https:%s_scan.txt' % j, 'w+b')
                    subprocess.call(['gobuster', '-w', '/usr/share/seclists/Discovery/Web-Content/common.txt','-u','https://'+sys.argv[2]+':'+check.group(3)], stdout=gob_https_log)

                    print ("[>] Activating Gobuster to run against HTTPS port: %s" % str(j))
                    nik_log = open('nikto_scan_%s.txt' % j, 'w+b')
                    subprocess.call(['nikto', '-h', 'https://'+sys.argv[2]+':'+check.group(3)], stdout=nik_log)

                else:
                    print("No HTTP(S) services found. Moving on to further scans...")

            except:
                check = None
            
            try:
                check_smb = re.search(
                    r'(^\d+)\/\w+\s+\w+\s+\w+?-\w+\s+(Samba)|(^\d+)\/\w+\s+\w+\s+(microsoft-ds)', line)
                
                if check_smb.group(2) == "Samba":
                    k = check_smb.group(1)

                    print(
                        "[>] Activating enum4linux to run against Samba port: %s" % str(k))
                    enum_Samba_log = open(
                        'enum4linux_Samba:%s_scan.txt' % k, 'w+b')
                    subprocess.call(['enum4linux',sys.argv[2]], stdout=enum_Samba_log)
                
                else:
                    print("No SMB or Samba services found. Moving on to further scans...")
                    
            except:
                check_smb = None

    with open("nmap_UDPscan.txt") as fnmap:
        for line in fnmap:
            try:
                check_snmp = re.search(
                    r'(^\d+)\/\w+\s+\w+\s+(snmp)', line)
                if check_snmp.group(2) == "snmp":
                    l = check_snmp.group(1)            
                    print(
                        "[>] Activating snmpwalk to run against SNMP port: %s" % str(l))
                    snmpwalk_log = open(
                        'snmpwalk:%s_scan.txt' % l, 'w+b')
                    subprocess.call(['snmpwalk', '-c', 'public', '-v1',sys.argv[2]], stdout=snmpwalk_log)
                
                else:
                    print("No SNMP services found. Moving on to further scans...")
                
            except:
                check_snmp = None

print ("[<] All scans are complete!")
print ("[<] You may check your current working directory for all the log files generated from the above scans")
print ("[<] Never Give Up and Try Harder!")
