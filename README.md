## Synopsis

Autoscan scans uses unicornscan to grab the open TCP/UDP ports, feeds them to NMAP scan and based on the output from NMAP, secondary scans are initiated(user intervention required): like gobuster and nikto for http(s), enum4linux for SMB/Samba and snmpwalk for snmp.

## Usage Example

* usage:   python3 autoscan_v2.py {interface} {IP adress}
* example: python3 autoscan_v2.py tap0 10.10.10.10

## Demo

[![asciicast](https://asciinema.org/a/xVmwNSTz0sNZ6WiedDK91B2Wu.png)](https://asciinema.org/a/xVmwNSTz0sNZ6WiedDK91B2Wu?autoplay=1)

## Motivation

Joshua Rahman, founder of [SECURISECCTF](https://securisec.github.io/) gave me this assignment to get my feet wet in Python3 and scale it as much as possible. He is my source of motivation for this project.

## Source of Idea

[superkojiman's onetwopunch.sh](https://github.com/superkojiman/onetwopunch)

## Dependency

This script is designed to work in Kali Linux because Kali Linux comes with Unicornscan, Nmap, gobuster, enum4linux, snmpwalk and nikto pre-installed

## Tests

Works perfectly fine against Offensive Security's OSCP lab or OSCP exam machines and Virtual-Machines from Vulnhub. Doesnt work with HTB(HackTheBox.eu) and the reason being Unicornscan fails to scan the HTB machines. If anyone knows a workaround feel free to contact me (b32)**PBQWK4TPMJXXEZ2AM5WWC2LMFZRW63IK**

## TODO
1. Find a way to make this script work against machines in HTB
2. Optimize this script even better



## Contributors

As of this time, "I" contribute to this script because I intend to learn and expand my Python programming skills

## License

I'll decide on this sooner

## Special Thanks
[Hackmethod](https://hackmethod.com/)
